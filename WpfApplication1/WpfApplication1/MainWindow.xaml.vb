﻿Class MainWindow 

    Private m_TreeViewItem As TreeViewItem

#Region "イベント"

    Private Sub 開く_Click(ByVal sender As System.Object, ByVal e As System.Windows.RoutedEventArgs) Handles 開く.Click
        Dim ofd = New Microsoft.Win32.OpenFileDialog()
        ofd.DefaultExt = ".html" ' Default file extension
        ofd.Filter = "HTMLファイル(*.html;*.htm)|*.html;*.htm;*.hta" ' Filter files by extension

        Dim result? As Boolean = ofd.ShowDialog()
        If result = True Then
            Dim filename As String = ofd.FileName
        End If
    End Sub

#End Region
   
#Region "ツリー関係"

    Private Sub ツリー_SelectedItemChanged(ByVal sender As System.Object, ByVal e As System.Windows.RoutedPropertyChangedEventArgs(Of System.Object)) Handles ツリー.SelectedItemChanged
        Me.m_TreeViewItem = e.NewValue
    End Sub

    Private Sub 枝追加_Click(ByVal sender As System.Object, ByVal e As System.Windows.RoutedEventArgs) Handles 枝追加.Click
        Dim 枝 As TreeViewItem = New TreeViewItem
        枝.Header = "枝" + (m_TreeViewItem.Items.Count + 1).ToString()
        枝.Name = "枝" + (m_TreeViewItem.Items.Count + 1).ToString()
        m_TreeViewItem.Items.Add(枝)
    End Sub

#End Region

    
    
End Class
