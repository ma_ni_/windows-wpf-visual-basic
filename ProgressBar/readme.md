# プログレスバー作成

---

### 目標 
* 処理している間にプログレスバーを表示する

### 現状
* UI操作をできるようにプログレスバー側で対応する場合、UIスレッドが占有されてしまう（Dialog1)
* プログレスバー側ではUIスレッドを占有しないように、バックグラウンドで実行する（Dialog2)
* UI操作を行う場合は、部分的にInvokeする。（Dialog3）
* バックグラウンド処理はタスクで処理した

---  
  
![img1](https://bitbucket.org/ma_ni/windows-wpf-visual-basic/raw/f840a23fbd7176d0838df0987eae273a9e87a5f9/ProgressBar/md_files/ss1.png)
![img1](https://bitbucket.org/ma_ni/windows-wpf-visual-basic/raw/a0e43ea0d5ca6349fa1d3efb6d15ed4886c5fa6f/ProgressBar/md_files/ss2.png)
> #### 今の機能
> * プログレスバー側でUI操作の考慮を行うことは不可能。UI操作は動作側で考慮する。
> * 計量UIスレッド機能を作成しなくても、タスクとTimerでうまく処理することができることがわかった。
> * いくつかの処理FileStreamのReadや、Tasks.Pararel.Forなどとは相性が悪い CPU消費が大きい処理？にはうまく機能しない？

> #### 今後やりたい機能
> * Tasks.Pararelのときにもうまく処理できるようにしたい

---

最終更新 2013年12月01日 (日) 1:48