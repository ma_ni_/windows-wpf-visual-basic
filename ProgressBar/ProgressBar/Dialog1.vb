﻿Imports System.Windows.Forms

Public Class Dialog1
    Private Shared s_action As Action()
    
    Private Sub New()
        InitializeComponent()
    End Sub

    Public Shared Sub ShowModalDialog(ByVal ParamArray action() As Action)
        s_action = action
        Dim d As Dialog1 = New Dialog1
        d.ShowDialog()
    End Sub

    Private Sub Dialog1_Shown(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Shown
        Me.ProgressBar1.Maximum = s_action.Count
        Me.ProgressBar1.Value = 0
        Me.ProgressBar1.Step = 1
        Me.Update()

        Me.ProgressBar1.BeginInvoke(New Action(Sub()
                                                   For i As Integer = 0 To s_action.Count - 1
                                                       Me.ProgressBar1.PerformStep()
                                                       Dim index = i
                                                       s_action(index)()
                                                   Next

                                                   s_action = Nothing
                                                   Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
                                               End Sub))

    End Sub

End Class
