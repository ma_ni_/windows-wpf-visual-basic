﻿
Public Structure actionStructure
    Public act As Action
    Public str As String
    Sub New(ByVal a As Action, ByVal s As String)
        act = a
        str = s
    End Sub
End Structure

Public Class Form1

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim acs As actionStructure() = Me.GetAction(True)
        Dim action_array(acs.Length - 1) As Action
        For index As Integer = 0 To acs.Length - 1
            action_array(index) = acs(index).act
        Next
        Dialog1.ShowModalDialog(action_array)
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Dialog2.ShowModalDialog(Me.GetAction(False))
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Dim action_array As actionStructure()
        action_array = Me.GetAction(False)

        Dim action_array_add(action_array.Length - 1) As actionStructure
        For index As Integer = 0 To action_array.Length - 1
            Dim lst As String = (index + 1).ToString()
            Dim index_ = index

            action_array_add(index) = New actionStructure(Sub()
                                                              Dim index__ = index_
                                                              Invoke(New Action(Sub()
                                                                                    Me.Label1.Text = lst
                                                                                    Me.Update()
                                                                                End Sub))
                                                              action_array(index__).act()
                                                          End Sub, _
                                      action_array(index).str)
        Next
        Dialog2.ShowModalDialog(action_array_add)
    End Sub

    Private Function GetAction(ByVal flg As Boolean) As actionStructure()
        Dim action_array(10) As actionStructure
        For index As Integer = 0 To 10
            Dim lst As String = (index + 1).ToString()
            action_array(index) = New actionStructure(Sub()
                                                          If flg Then
                                                              Me.Label1.Text = lst
                                                              Me.Update()
                                                          End If

                                                          System.Threading.Thread.Sleep(2500)
                                                      End Sub, _
                                                       "アクションNo." + index.ToString())
        Next

        Return action_array
    End Function


End Class
