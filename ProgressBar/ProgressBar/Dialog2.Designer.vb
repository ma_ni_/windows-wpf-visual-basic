﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Dialog2
    Inherits System.Windows.Forms.Form

    'フォームがコンポーネントの一覧をクリーンアップするために dispose をオーバーライドします。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Windows フォーム デザイナーで必要です。
    Private components As System.ComponentModel.IContainer

    'メモ: 以下のプロシージャは Windows フォーム デザイナーで必要です。
    'Windows フォーム デザイナーを使用して変更できます。  
    'コード エディターを使って変更しないでください。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.ProgressBar1 = New System.Windows.Forms.ProgressBar()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label_Time = New System.Windows.Forms.Label()
        Me.Label_Do = New System.Windows.Forms.Label()
        Me.Label_XN = New System.Windows.Forms.Label()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.SuspendLayout()
        '
        'ProgressBar1
        '
        Me.ProgressBar1.Location = New System.Drawing.Point(12, 12)
        Me.ProgressBar1.Maximum = 11
        Me.ProgressBar1.Name = "ProgressBar1"
        Me.ProgressBar1.Size = New System.Drawing.Size(411, 23)
        Me.ProgressBar1.TabIndex = 3
        Me.ProgressBar1.Value = 10
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(13, 42)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(53, 12)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "経過時間"
        '
        'Label_Time
        '
        Me.Label_Time.AutoSize = True
        Me.Label_Time.Location = New System.Drawing.Point(358, 42)
        Me.Label_Time.Name = "Label_Time"
        Me.Label_Time.Size = New System.Drawing.Size(65, 12)
        Me.Label_Time.TabIndex = 5
        Me.Label_Time.Text = "00:00:00.000"
        '
        'Label_Do
        '
        Me.Label_Do.AutoSize = True
        Me.Label_Do.Location = New System.Drawing.Point(13, 56)
        Me.Label_Do.Name = "Label_Do"
        Me.Label_Do.Size = New System.Drawing.Size(53, 12)
        Me.Label_Do.TabIndex = 6
        Me.Label_Do.Text = "～処理中"
        '
        'Label_XN
        '
        Me.Label_XN.AutoSize = True
        Me.Label_XN.Location = New System.Drawing.Point(389, 56)
        Me.Label_XN.Name = "Label_XN"
        Me.Label_XN.Size = New System.Drawing.Size(34, 12)
        Me.Label_XN.TabIndex = 7
        Me.Label_XN.Text = "X / N"
        '
        'Timer1
        '
        Me.Timer1.Interval = 1
        '
        'Dialog2
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(435, 77)
        Me.Controls.Add(Me.Label_XN)
        Me.Controls.Add(Me.Label_Do)
        Me.Controls.Add(Me.Label_Time)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.ProgressBar1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "Dialog2"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Dialog2"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ProgressBar1 As System.Windows.Forms.ProgressBar
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label_Time As System.Windows.Forms.Label
    Friend WithEvents Label_Do As System.Windows.Forms.Label
    Friend WithEvents Label_XN As System.Windows.Forms.Label
    Friend WithEvents Timer1 As System.Windows.Forms.Timer

End Class
