﻿Imports System.Windows.Forms

Public Class Dialog2
    Private Shared s_as As actionStructure()

    Private m_cts As System.Threading.CancellationTokenSource
    Private m_ts As System.Threading.Tasks.TaskScheduler
    Private m_st As Date

    Private Sub New()
        InitializeComponent()
    End Sub

    Public Shared Sub ShowModalDialog(ByVal ParamArray acts() As actionStructure)
        s_as = acts
        Dim d As Dialog2 = New Dialog2
        d.ShowDialog()
    End Sub

    Private Sub Dialog2_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        Me.m_cts.Cancel()
        Me.Timer1.Stop()
    End Sub

    Private Sub Dialog2_Shown(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Shown
        Me.ProgressBar1.Maximum = s_as.Count
        Me.ProgressBar1.Value = 0
        Me.ProgressBar1.Step = 1
        Me.Update()

        Me.m_cts = New System.Threading.CancellationTokenSource
        Me.m_st = Date.Now

        Me.Timer1.Start()

        m_ts = System.Threading.Tasks.TaskScheduler.FromCurrentSynchronizationContext()
        System.Threading.Tasks.Task.Factory.StartNew(Sub()
                                                         Me.UiSub(Sub()
                                                                      Me.Label_XN.Text = String.Format("0 / {0}", (s_as.Count - 1).ToString)
                                                                  End Sub)

                                                         For i As Integer = 0 To s_as.Count - 1
                                                             Dim index = i

                                                             Me.UiSub(Sub()
                                                                          Me.ProgressBar1.PerformStep()
                                                                          Me.Label_Do.Text = s_as(index).str + "処理中"

                                                                      End Sub)
                                                             If Me.m_cts.Token.IsCancellationRequested <> True Then
                                                                 s_as(index).act()
                                                             End If
                                                             Me.UiSub(Sub()
                                                                          Me.Label_XN.Text = String.Format("{0} / {1}", index.ToString, (s_as.Count - 1).ToString)
                                                                      End Sub)
                                                         Next

                                                         Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
                                                     End Sub)
    End Sub

    Private Sub UiSub(ByVal act As Action)
        System.Threading.Tasks.Task.Factory.StartNew(Sub()
                                                         If Me.m_cts.Token.IsCancellationRequested Then
                                                             Me.m_cts.Token.ThrowIfCancellationRequested()
                                                         Else
                                                             act()
                                                             Me.Update()
                                                         End If
                                                     End Sub, _
                                                     Me.m_cts.Token, _
                                                     Threading.Tasks.TaskCreationOptions.PreferFairness, _
                                                     m_ts)
    End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        Dim d As Date = Date.Now
        Dim span As TimeSpan = d - Me.m_st
        Me.Label_Time.Text = String.Format("{0:00}:{1:00}:{2:00}.{3:000}", span.Hours, span.Minutes, span.Seconds, span.Milliseconds)
        Me.Update()
    End Sub

End Class
